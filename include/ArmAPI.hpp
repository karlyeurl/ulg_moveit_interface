#include "ros/ros.h"
#include <moveit/move_group_interface/move_group.h>

/** 
  * This structure is used to describe the position and orientation of a 3D object.
  * @px, @py and @pz are the cartesian coordinates of the object, 
  * @ox, @oy, @oz and @ow are the parameters of the quaternion describing its orientation.
  *
  * Can be constructed directly with a pose.
**/
struct pos3D {
   double px;
   double py;
   double pz;
   double ow;
   double ox;
   double oy;
   double oz;
   pos3D(double posx, double posy, double posz, double orw, double orx, double ory, double orz) :
      px(posx), 
      py(posy), 
      pz(posz), 
      ow(orw), 
      ox(orx), 
      oy(ory), 
      oz(orz) 
    {};
    pos3D(){};
};

class Arm {
   public:
   Arm();

   //TODO add other generic objects
   // Adds boxes to the environment for collision avoidance.
   void addCube(pos3D pos, double edge, const std::string name);
   void addCube(pos3D pos, double edge1, double edge2, double edge3, const std::string name);

   // Attaches the box to the end effector (the box will move with the arm)
   void attachCube(pos3D pos, double edge, const std::string name);
   void attachCube(const std::string name);

   // Detaches an attached box
   void detachCube(const std::string name);

   // Deletes the box from the environment
   void removeCube(const std::string name);


   //TODO could add as well the possibility to set the planning time
   // Moves the arm to the given pose. Returns before completion.
   bool moveTo(pos3D pos);
   bool moveTo(pos3D pos, int retries);//Warning: retries is the number of REtries! 0 allows the same behavior than moveTo(pos3D pos);
   bool moveTo(const std::string name);//only "home" is supported yet

   //Joint targets have to be given in the following order: shoulder_pan_joint, shoulder_lift_joint, elbow_joint, wrist_1_joint, wrist_2_joint, wrist_3_joint
   bool moveJointsTo(const std::vector<double>& targets);

   // Moves the arm to the given pose. Returns after completion.
   bool moveToSync(pos3D pos, int retries);

   // Moves the arm along a cartesian path. For small movements.
   bool moveCartesian(std::vector<pos3D> pos);



   // Planning/execution methods the grasping approach.
   int planFirstPhase(pos3D* pos);
   void executeFirstPhase();
   double planSecondPhase(pos3D* pos);
   void executeSecondPhase();

   /*↓ DO NOT USE DO NOT USE (yet) ↓*/
   double planApproach(pos3D* pos);
   void graspingApproach(pos3D* pos);// Warning: makes the arm move in a synchronous manner, even if the cartesian path cannot be completed.
   /*↑ DO NOT USE DO NOT USE (yet) ↑*/

   // Stops the execution. Only works when the movement is asynchroneous
   void stop();

   // Returns the pose of the end_effector
   void getPos(pos3D* pos);
   // Returns the values of the joints
   void getJoints(std::vector<double>& joints);

   //dtor
   ~Arm();

   private:
   ros::Publisher attached_object_publisher;
   ros::Publisher collision_object_publisher;
   ros::AsyncSpinner& getSpinner();
   move_group_interface::MoveGroup& getMoveGroup();
   move_group_interface::MoveGroup& getMoveGroupAlt();
   move_group_interface::MoveGroup::Plan& getApproachPlan();
   move_group_interface::MoveGroup::Plan& getCartesianPlan();

   pos3D& getBackedOffPose(pos3D* pos);
};
