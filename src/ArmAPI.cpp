#include "ArmAPI.hpp"
#include "util.h"
#include "std_msgs/String.h"
#include <sstream>
#include <moveit/move_group_interface/move_group.h>
#include <moveit/robot_trajectory/robot_trajectory.h>
#include <moveit/trajectory_processing/trajectory_tools.h>
#include <moveit/trajectory_processing/iterative_time_parameterization.h>
#include <moveit/trajectory_execution_manager/trajectory_execution_manager.h>

Arm::Arm() {
   int argc = 0;
   char **argv = NULL;
   ros::init(argc, argv, "UR5ArmAPI", ros::init_options::AnonymousName);
   ros::NodeHandle n;
   ros::AsyncSpinner spinner = getSpinner();
   spinner.start();


   /* Publisher for attached objects */
   attached_object_publisher = n.advertise<moveit_msgs::AttachedCollisionObject>("attached_collision_object", 1);
   while(attached_object_publisher.getNumSubscribers() < 1){
      ros::WallDuration sleep_t(0.5);
      sleep_t.sleep();    
   }

   /* Publisher for collision objects */
   collision_object_publisher = n.advertise<moveit_msgs::CollisionObject>("collision_object", 1);
   while(collision_object_publisher.getNumSubscribers() < 1){
      ros::WallDuration sleep_t(0.5);
      sleep_t.sleep();    
   }

   move_group_interface::MoveGroup& g = getMoveGroup(); //initializes the move group
   g.allowReplanning(true); 
   move_group_interface::MoveGroup& galt = getMoveGroupAlt(); //initializes the move group alt
   galt.allowReplanning(true); 

}

ros::AsyncSpinner& Arm::getSpinner() {
   static ros::AsyncSpinner spinner(2);
   return spinner;
}

move_group_interface::MoveGroup& Arm::getMoveGroup() {
   static move_group_interface::MoveGroup group("manipulator");
   return group;
}

move_group_interface::MoveGroup& Arm::getMoveGroupAlt() {
   static move_group_interface::MoveGroup group_alt("manipulator");
//   std::cout << "USING THE ALTERNATE MOVE GROUP INSTANCE" << std::endl;
   return group_alt;
}

void Arm::addCube(pos3D pos, double edge, const std::string name) {
   addCube(pos, edge, edge, edge, name);
}

void Arm::addCube(pos3D pos, double edge1, double edge2, double edge3, const std::string name) {
   move_group_interface::MoveGroup& group = getMoveGroup();
   if (collision_object_publisher.getNumSubscribers() < 1) {
      ROS_INFO("%s", "Couldn't add a cube because nobody's listening.");
      return;
   }

   // Create the new object //
   moveit_msgs::CollisionObject new_object;
   new_object.id = name;
   new_object.header.frame_id = "/torso";  
   new_object.operation = new_object.ADD;

   // Prepare the position //
   geometry_msgs::Pose pose;
   pose.position.x = pos.px;
   pose.position.y = pos.py;
   pose.position.z = pos.pz;
   pose.orientation.x = pos.ox;
   pose.orientation.y = pos.oy;
   pose.orientation.z = pos.oz;
   pose.orientation.w = pos.ow;

   // Hack to get the right structure //
   shape_msgs::SolidPrimitive primitive;
   primitive.type = primitive.BOX;  
   primitive.dimensions.resize(3);
   primitive.dimensions[0] = edge1; //TODO particularize this huhu
   primitive.dimensions[1] = edge2;
   primitive.dimensions[2] = edge3;

   // Pushing data into the object //
   new_object.primitives.push_back(primitive);
   new_object.primitive_poses.push_back(pose);

   // Add it to the scene //
   collision_object_publisher.publish(new_object);
}

void Arm::attachCube(const std::string name) {
   move_group_interface::MoveGroup& group = getMoveGroup();
   if (attached_object_publisher.getNumSubscribers() < 1) {
      ROS_INFO("%s", "Couldn't attach a cube because nobody's listening.");
      return;
   }
   moveit_msgs::AttachedCollisionObject attach_object;
   attach_object.object.id = name;
   attach_object.link_name = "ee_link"; //TODO surcharge for other link name
   attach_object.object.operation = attach_object.object.ADD;
   attached_object_publisher.publish(attach_object);
}

void Arm::attachCube(pos3D pos, double edge, const std::string name) {
   addCube(pos, edge, name);
   attachCube(name);
}

void Arm::detachCube(const std::string name) {
   move_group_interface::MoveGroup& group = getMoveGroup();
   if (attached_object_publisher.getNumSubscribers() < 1) {
      ROS_INFO("%s", "Couldn't detach a cube because nobody's listening.");
      return;
   }
   moveit_msgs::AttachedCollisionObject detach_object;
   detach_object.object.id = name;
   detach_object.link_name = "ee_link"; //TODO surcharge for other link name (needed?)
   detach_object.object.operation = detach_object.object.REMOVE;
   attached_object_publisher.publish(detach_object);
}

void Arm::removeCube(const std::string name) {
   move_group_interface::MoveGroup& group = getMoveGroup();
   if (collision_object_publisher.getNumSubscribers() < 1) {
      ROS_INFO("%s", "Couldn't remove a cube because nobody's listening.");
      return;
   }
   moveit_msgs::CollisionObject remove_object;
   remove_object.id = name;
   //remove_object.link_name = "ee_link"; //TODO surcharge for other link name (needed?)
   remove_object.operation = remove_object.REMOVE;
   collision_object_publisher.publish(remove_object);
}

bool Arm::moveTo(pos3D pos) {
   return moveTo(pos, 0);
}

bool Arm::moveTo(pos3D pos, int retries) {
   move_group_interface::MoveGroup& group = getMoveGroup();
   group.setPlanningTime(5.0);
   //ros::AsyncSpinner spinner(1);
   //spinner.start();
   // Security measure, tolerances //
   group.setStartStateToCurrentState();
   group.setGoalPositionTolerance(0.0001);
   group.setGoalOrientationTolerance(0.0001);

   // Trick to avoid building a structure from scratch //
   geometry_msgs::PoseStamped pose = group.getCurrentPose();
   pose.pose.position.x = pos.px;
   pose.pose.position.y = pos.py;
   pose.pose.position.z = pos.pz;
   pose.pose.orientation.x = pos.ox;
   pose.pose.orientation.y = pos.oy;
   pose.pose.orientation.z = pos.oz;
   pose.pose.orientation.w = pos.ow;
   //TODO fix me plz, its a hax. Should not be needed, appears to be needed.
   group.setPoseReferenceFrame(group.getPoseReferenceFrame());
   group.setPoseTarget(pose);
   group.setPoseReferenceFrame(group.getPoseReferenceFrame());

   // Planning and executing //
   move_group_interface::MoveGroup::Plan plan;
   int i = 0;
   while (i <= retries && !group.plan(plan)) {
      i++;
   }
   if (i <= retries) {
      group.asyncExecute(plan);
   }
   return (i <= retries); // True iif group.asyncExecute succeeded
}

bool Arm::moveTo(const std::string name) {
   move_group_interface::MoveGroup& group = getMoveGroup();
   group.setPlanningTime(5.0);
   //ros::AsyncSpinner spinner(1);
   //spinner.start();
   // Security measure, tolerances //
   group.setStartStateToCurrentState();
   group.setGoalPositionTolerance(0.0001);
   group.setGoalOrientationTolerance(0.0001);

   group.setNamedTarget(name);

   // Planning and executing //
   move_group_interface::MoveGroup::Plan plan;

   int i = 0;
   while (i <= 3 && !group.plan(plan)) {
      i++;
   }
   if (i <= 3) {
      group.asyncExecute(plan);
   }
   return (i <= 3); // True iif group.asyncExecute succeeded
}

bool Arm::moveJointsTo(const std::vector<double>& targets) {
   move_group_interface::MoveGroup& group = getMoveGroup();
   group.setPlanningTime(5.0);
   //ros::AsyncSpinner spinner(1);
   //spinner.start();
   group.setStartStateToCurrentState();
   group.setGoalPositionTolerance(0.0001);
   group.setGoalOrientationTolerance(0.0001);

   if (targets.size() !=6 || !group.setJointValueTarget(targets)) {
      ROS_INFO("Couldn't set the targets. %s", (targets.size()!=6)?"Wrong target size (vector must contain 6 elements).":"Invalid joint values.");
      return false;
   }

   move_group_interface::MoveGroup::Plan plan;
   int i = 0;
   while (i <= 3 && !group.plan(plan)) {
      i++;
   }
   if (i <= 3) {
      group.execute(plan);
   }
   return (i <= 3);
}

move_group_interface::MoveGroup::Plan& Arm::getApproachPlan() {
   static move_group_interface::MoveGroup::Plan plan;
   return plan;
}

move_group_interface::MoveGroup::Plan& Arm::getCartesianPlan() {
   static move_group_interface::MoveGroup::Plan plan;
   return plan;
}


bool Arm::moveToSync(pos3D pos, int retries) {
   move_group_interface::MoveGroup& group = getMoveGroup();
   group.setPlanningTime(10.0);
   group.setStartStateToCurrentState();
   //ros::AsyncSpinner spinner(1);
   //spinner.start();
   // Security measure, tolerances //
   group.setStartStateToCurrentState();
   group.setGoalPositionTolerance(0.0001);
   group.setGoalOrientationTolerance(0.0001);

   // Trick to avoid building a structure from scratch //
   geometry_msgs::PoseStamped pose = group.getCurrentPose();
   pose.pose.position.x = pos.px;
   pose.pose.position.y = pos.py;
   pose.pose.position.z = pos.pz;
   pose.pose.orientation.x = pos.ox;
   pose.pose.orientation.y = pos.oy;
   pose.pose.orientation.z = pos.oz;
   pose.pose.orientation.w = pos.ow;
   //TODO fix me plz, its a hax. Should not be needed, appears to be needed.
   group.setPoseReferenceFrame(group.getPoseReferenceFrame());
   group.setPoseTarget(pose);
   group.setPoseReferenceFrame(group.getPoseReferenceFrame());

   // Planning and executing //
   move_group_interface::MoveGroup::Plan plan;
   group.plan(plan);
   int i = 0;
   while (i <= retries && !group.execute(plan)) {
      i++;
   }
   return (i <= retries); // True iif group.asyncExecute succeeded
}

bool Arm::moveCartesian(std::vector<pos3D> positions) {
   move_group_interface::MoveGroup& group = getMoveGroup();
   group.setPlanningTime(5.0);
   //ros::AsyncSpinner spinner(1);
   //spinner.start();

   moveit::planning_interface::MoveGroup::Plan plan;
   std::vector<geometry_msgs::Pose> waypoints;
   geometry_msgs::Pose target_pose = group.getCurrentPose().pose;

   for (std::vector<pos3D>::iterator pos = positions.begin() ; pos != positions.end(); ++pos) {
      target_pose.position.x = pos->px;
      target_pose.position.y = pos->py;
      target_pose.position.z = pos->pz;
      target_pose.orientation.w = pos->ow;
      target_pose.orientation.x = pos->ox;
      target_pose.orientation.y = pos->oy;
      target_pose.orientation.z = pos->oz;
      waypoints.push_back(target_pose);
   }
   moveit_msgs::RobotTrajectory trajectory_msg;
   group.setPlanningTime(10.0);
   double fraction = group.computeCartesianPath(waypoints,
                                        0.01, // eef_step
                                        0.0,  // jump_threshold
                                        trajectory_msg, true);


   // The trajectory needs to be modified so it will include velocities as well.
   // First create a RobotTrajectory object
   robot_trajectory::RobotTrajectory rt(group.getCurrentState()->getRobotModel(), "manipulator");

   // Second get a RobotTrajectory from trajectory
   rt.setRobotTrajectoryMsg(*group.getCurrentState(), trajectory_msg);
 
   // Thrid create a IterativeParabolicTimeParameterization object
   trajectory_processing::IterativeParabolicTimeParameterization iptp;
   // Fourth compute computeTimeStamps
   bool success = iptp.computeTimeStamps(rt);
   ROS_INFO("Computed time stamp %s",success?"SUCCEDED":"FAILED");
   // Get RobotTrajectory_msg from RobotTrajectory
   rt.getRobotTrajectoryMsg(trajectory_msg);



   //std::cout << trajectory_msg << std::endl;
   plan.trajectory_ = trajectory_msg;
   ROS_INFO("Visualizing plan 4 (cartesian path) (%.2f%% acheived)",fraction * 100.0);

   if (success && fraction > 0.9) {
      group.asyncExecute(plan);
      return fraction;
   }

   return false;
}


void Arm::getPos(pos3D* pos) {
   move_group_interface::MoveGroup& group = getMoveGroup();
   geometry_msgs::PoseStamped pose = group.getCurrentPose();
   pos->px = pose.pose.position.x;
   pos->py = pose.pose.position.y;
   pos->pz = pose.pose.position.z;
   pos->ow = pose.pose.orientation.w;
   pos->ox = pose.pose.orientation.x;
   pos->oy = pose.pose.orientation.y;
   pos->oz = pose.pose.orientation.z;
}


void Arm::getJoints(std::vector<double>& joints) {
   move_group_interface::MoveGroup& group = getMoveGroup();
   joints = group.getCurrentJointValues();
}


void Arm::stop() {
   move_group_interface::MoveGroup& group = getMoveGroup();
   group.stop();
}


int Arm::planFirstPhase(pos3D* pos) {
   /* 0: Instantiation of the variables */
   move_group_interface::MoveGroup& group = getMoveGroup();
   
   /* 1: Add the box */
   graspsym::PoseQuat goal1;
   goal1 << pos->px, pos->py, pos->pz, pos->ow, pos->ox, pos->oy, pos->oz;

   graspsym::PoseMat goal1_mat;

   float cos45 = 0.7071;
   graspsym::PoseMat obstacle;
   obstacle << 1, 0, 0, 0.27,
             0, 1, 0, 0,
             0, 0, 1, 0,
             0, 0, 0, 1;
  
   graspsym::poseQuatToPoseMat(goal1_mat, goal1);
  
   graspsym::PoseMat obstaclePose;
   graspsym::PoseQuat obstaclePose_Quat;

   obstaclePose = goal1_mat * obstacle;
   graspsym::poseMatToPoseQuat (obstaclePose_Quat, obstaclePose);
   pos3D box_obstacle;
   box_obstacle.px = obstaclePose_Quat(0, 0);
   box_obstacle.py = obstaclePose_Quat(1, 0);
   box_obstacle.pz = obstaclePose_Quat(2, 0);
   box_obstacle.ow = 1;
   box_obstacle.ox = 0;
   box_obstacle.oy = 0;
   box_obstacle.oz = 0;

   addCube(box_obstacle, 0.08, 0.08, 0.30, "obstacle_grasp");
   
   /* 2: Compute the backed off pose */
   graspsym::PoseMat backOff;
   backOff << 1, 0, 0, -0.1,
             0, 1, 0, 0,
             0, 0, 1, 0,
             0, 0, 0, 1;
  
   graspsym::PoseMat backedOffPose;
   graspsym::PoseQuat backedOffPose_Quat;

   backedOffPose = goal1_mat * backOff;
   graspsym::poseMatToPoseQuat (backedOffPose_Quat, backedOffPose);

   /*std::cout << "Goal is: " << goal1_mat << std::endl << std::endl;
   std::cout << goal1 << std::endl << std::endl;

   std::cout << "Intermediate position: " << backedOffPose << std::endl << std::endl;
   std::cout << backedOffPose_Quat << std::endl;*/

   pos3D bo;
   bo.px = backedOffPose_Quat(0, 0);
   bo.py = backedOffPose_Quat(1, 0);
   bo.pz = backedOffPose_Quat(2, 0);
   bo.ow = backedOffPose_Quat(3, 0);
   bo.ox = backedOffPose_Quat(4, 0);
   bo.oy = backedOffPose_Quat(5, 0);
   bo.oz = backedOffPose_Quat(6, 0);

   /* 3: COMPUTE the trajectory */
   group.setStartStateToCurrentState();
   group.setGoalPositionTolerance(0.0001);
   group.setGoalOrientationTolerance(0.0001);

   geometry_msgs::PoseStamped pose = group.getCurrentPose();
   pose.pose.position.x = bo.px;
   pose.pose.position.y = bo.py;
   pose.pose.position.z = bo.pz;
   pose.pose.orientation.x = bo.ox;
   pose.pose.orientation.y = bo.oy;
   pose.pose.orientation.z = bo.oz;
   pose.pose.orientation.w = bo.ow;

   //TODO fix me plz, its a hax. Should not be needed, appears to be needed.
   group.setPoseReferenceFrame(group.getPoseReferenceFrame());
   group.setPoseTarget(pose);
   group.setPoseReferenceFrame(group.getPoseReferenceFrame());

   // Planning //

   move_group_interface::MoveGroup::Plan& plan = getApproachPlan();
   int i = 0;
   group.setPlanningTime(10.0);
   while (i <= 5 && !group.plan(plan)) {
      i++;
   }
   /* 4: Remove the box and exit if no motion plan is found */

   removeCube("obstacle_grasp");

   if (i > 5) {
      return -1; //no motion plan has been found for the backed off pose
   }
   return 0;
}


// warning sync method
void Arm::executeFirstPhase() {
   move_group_interface::MoveGroup& group = getMoveGroup();
   group.setPlanningTime(5.0);
   move_group_interface::MoveGroup::Plan& plan = getApproachPlan();

   /*int i = 0;
   while (i <= 5 && !group.execute(plan)) {
      i++;
   }*/
   group.execute(plan);
   return;
}

// /!\ need to have executed the first phase //
double Arm::planSecondPhase(pos3D* pos) {
   ///compute the cartesian approach

   move_group_interface::MoveGroup& group = getMoveGroup();
   group.setPlanningTime(5.0);
   move_group_interface::MoveGroup::Plan& plan = getCartesianPlan();


   group.setStartStateToCurrentState();
   group.setGoalPositionTolerance(0.0001);
   group.setGoalOrientationTolerance(0.0001);

   std::vector<geometry_msgs::Pose> waypoints;
   geometry_msgs::Pose target_pose = group.getCurrentPose().pose;
   waypoints.push_back(target_pose);

   target_pose.position.x = pos->px;
   target_pose.position.y = pos->py;
   target_pose.position.z = pos->pz;
   target_pose.orientation.w = pos->ow;
   target_pose.orientation.x = pos->ox;
   target_pose.orientation.y = pos->oy;
   target_pose.orientation.z = pos->oz;
   
   waypoints.push_back(target_pose);


   moveit_msgs::RobotTrajectory trajectory_msg;
   group.setPlanningTime(10.0);
   double fraction = group.computeCartesianPath(waypoints,
                                        0.01, // eef_step
                                        0.0,  // jump_threshold
                                        trajectory_msg, true);


   // The trajectory needs to be modified so it will include velocities as well.
   // First create a RobotTrajectory object
   robot_trajectory::RobotTrajectory rt(group.getCurrentState()->getRobotModel(), "manipulator");

   // Second get a RobotTrajectory from trajectory
   rt.setRobotTrajectoryMsg(*group.getCurrentState(), trajectory_msg);
 
   // Thrid create a IterativeParabolicTimeParameterization object
   trajectory_processing::IterativeParabolicTimeParameterization iptp;
   // Fourth compute computeTimeStamps
   bool success = iptp.computeTimeStamps(rt);
   ROS_INFO("Computed time stamp %s",success?"SUCCEDED":"FAILED");
   // Get RobotTrajectory_msg from RobotTrajectory
   rt.getRobotTrajectoryMsg(trajectory_msg);



   plan.trajectory_ = trajectory_msg;
   ROS_INFO("Visualizing plan 4 (cartesian path) (%.2f%% acheived)",fraction * 100.0);

   return fraction;
}

void Arm::executeSecondPhase() {
   move_group_interface::MoveGroup& group = getMoveGroup();
   group.setPlanningTime(5.0);
   move_group_interface::MoveGroup::Plan& plan = getCartesianPlan();

   int i = 0;
   while (i <= 5 && !group.asyncExecute(plan)) {
      i++;
   }
   return;
}

/* This function is still a WIP */
double Arm::planApproach(pos3D* pos) {
   /* 0: Instantiation of the variables */
   move_group_interface::MoveGroup& group = getMoveGroup();
   //ros::AsyncSpinner spinner(1);
   //spinner.start();
   

   /* 1: Add the box */
   graspsym::PoseQuat goal1;
   goal1 << pos->px, pos->py, pos->pz, pos->ow, pos->ox, pos->oy, pos->oz;

   graspsym::PoseMat goal1_mat;

   float cos45 = 0.7071;
   graspsym::PoseMat obstacle;
   obstacle << 1, 0, 0, 0.27,
             0, 1, 0, 0,
             0, 0, 1, 0,
             0, 0, 0, 1;
  
   //poseEulerToPoseMat(Ur5, poseUr5);
   graspsym::poseQuatToPoseMat(goal1_mat, goal1);
  
   graspsym::PoseMat obstaclePose;
   graspsym::PoseQuat obstaclePose_Quat;

   obstaclePose = goal1_mat * obstacle;
   graspsym::poseMatToPoseQuat (obstaclePose_Quat, obstaclePose);
   pos3D box_obstacle;
   box_obstacle.px = obstaclePose_Quat(0, 0);
   box_obstacle.py = obstaclePose_Quat(1, 0);
   box_obstacle.pz = obstaclePose_Quat(2, 0);
   box_obstacle.ow = 1;
   box_obstacle.ox = 0;
   box_obstacle.oy = 0;
   box_obstacle.oz = 0;

   addCube(box_obstacle, 0.09, 0.09, 0.30, "obstacle_grasp");
   
   /* 2: Compute the backed off pose */
   graspsym::PoseMat backOff;
   backOff << 1, 0, 0, -0.1,
             0, 1, 0, 0,
             0, 0, 1, 0,
             0, 0, 0, 1;
  
   graspsym::PoseMat backedOffPose;
   graspsym::PoseQuat backedOffPose_Quat;

   backedOffPose = goal1_mat * backOff;
   graspsym::poseMatToPoseQuat (backedOffPose_Quat, backedOffPose);

   /*std::cout << "Goal is: " << goal1_mat << std::endl << std::endl;
   std::cout << goal1 << std::endl << std::endl;

   std::cout << "Intermediate position: " << backedOffPose << std::endl << std::endl;
   std::cout << backedOffPose_Quat << std::endl;*/

   pos3D bo;
   bo.px = backedOffPose_Quat(0, 0);
   bo.py = backedOffPose_Quat(1, 0);
   bo.pz = backedOffPose_Quat(2, 0);
   bo.ow = backedOffPose_Quat(3, 0);
   bo.ox = backedOffPose_Quat(4, 0);
   bo.oy = backedOffPose_Quat(5, 0);
   bo.oz = backedOffPose_Quat(6, 0);

   /* 3: COMPUTE the trajectory */
   group.setStartStateToCurrentState();
   group.setGoalPositionTolerance(0.0001);
   group.setGoalOrientationTolerance(0.0001);

   geometry_msgs::PoseStamped pose = group.getCurrentPose();
   pose.pose.position.x = bo.px;
   pose.pose.position.y = bo.py;
   pose.pose.position.z = bo.pz;
   pose.pose.orientation.x = bo.ox;
   pose.pose.orientation.y = bo.oy;
   pose.pose.orientation.z = bo.oz;
   pose.pose.orientation.w = bo.ow;

   //TODO fix me plz, its a hax. Should not be needed, appears to be needed.
   group.setPoseReferenceFrame(group.getPoseReferenceFrame());
   group.setPoseTarget(pose);
   group.setPoseReferenceFrame(group.getPoseReferenceFrame());

   // Planning //

   move_group_interface::MoveGroup::Plan& plan = getApproachPlan();
   int i = 0;
   group.setPlanningTime(10.0);
   while (i <= 5 && !group.plan(plan)) {
      i++;
   }
   /* 4: Remove the box and exit if no motion plan is found */

   removeCube("obstacle_grasp");

   if (i > 5) {
      return -1; //no motion plan has been found for the backed off pose
   }

   moveit_msgs::RobotTrajectory first_traj_msg = plan.trajectory_;
   robot_trajectory::RobotTrajectory first_traj(group.getCurrentState()->getRobotModel(), "manipulator");
   first_traj.setRobotTrajectoryMsg(*group.getCurrentState(), first_traj_msg);
   const robot_state::RobotState& intermediate_state = first_traj.getLastWayPoint();
   group.setStartState(intermediate_state);

   /* 5: COMPUTE the cartesian trajectory from the backed off pose to the goal */
   /* SHOULD RETURN HERE */
   std::vector<pos3D> positions;
   //getPos(&bo);
   positions.push_back(bo);

   //removeCube("obstacle_grasp");

   pos3D final_pos(pos->px,pos->py,pos->pz,pos->ow,pos->ox,pos->oy,pos->oz);
   positions.push_back(final_pos);


   std::vector<geometry_msgs::Pose> waypoints;
   geometry_msgs::Pose target_pose = group.getCurrentPose().pose;

   for (std::vector<pos3D>::iterator pos = positions.begin() ; pos != positions.end(); ++pos) {
      target_pose.position.x = pos->px;
      target_pose.position.y = pos->py;
      target_pose.position.z = pos->pz;
      target_pose.orientation.w = pos->ow;
      target_pose.orientation.x = pos->ox;
      target_pose.orientation.y = pos->oy;
      target_pose.orientation.z = pos->oz;
      waypoints.push_back(target_pose);
   }

   /*for (std::vector<geometry_msgs::Pose>::iterator pos = waypoints.begin() ; pos != waypoints.end(); ++pos) {
      std::cout << "Waypoints: \n" << *pos << std::endl << std::endl;
   }*/


   moveit_msgs::RobotTrajectory trajectory_msg;

   group.setPlanningTime(10.0);
   double fraction = group.computeCartesianPath(waypoints,
                                        0.01, // eef_step
                                        0.0,  // jump_threshold
                                        trajectory_msg, true);


   //std::cout << trajectory_msg << std::endl;

   // The trajectory needs to be modified so it will include velocities as well.
   // First create a RobotTrajectory object
   robot_trajectory::RobotTrajectory rt(group.getCurrentState()->getRobotModel(), "manipulator");

   // Second get a RobotTrajectory from trajectory
   rt.setRobotTrajectoryMsg(*group.getCurrentState(), trajectory_msg);
 
   // Thrid create a IterativeParabolicTimeParameterization object
   trajectory_processing::IterativeParabolicTimeParameterization iptp;
   // Fourth compute computeTimeStamps
   bool success = iptp.computeTimeStamps(rt);
   ROS_INFO("Computed time stamp %s",success?"SUCCEDED":"FAILED");
   // Get RobotTrajectory_msg from RobotTrajectory
   rt.getRobotTrajectoryMsg(trajectory_msg);



   //std::cout << trajectory_msg << std::endl;
   move_group_interface::MoveGroup::Plan& cplan = getCartesianPlan();
  
   cplan.trajectory_ = trajectory_msg;
   ROS_INFO("Visualizing plan 4 (cartesian path) (%.2f%% acheived)",fraction * 100.0);

   /*if (success && fraction > 0.9) {
      group.asyncExecute(cplan);
      return fraction;
   }*/
   group.setStartStateToCurrentState();

   return fraction;



   //TODO

   /* 6: Return the success value: -1 for failed traj1, then 0→100 for the cartesian traj. */




   /* Execution function: Arm::executeLastPlannedApproach() */
   /* If the last computed values are good, execute first synchroneously, then test with async */
}


// /!\ Uses a synchroneous method. Be careful in case of the need of an emergency brake.
void Arm::graspingApproach(pos3D* pos) {
   move_group_interface::MoveGroup& group = getMoveGroup();
   //ros::AsyncSpinner spinner(1);
   //spinner.start();

   /* 0: compute the pose of the cube and add it to the env. */
   graspsym::PoseQuat goal1;
   goal1 << pos->px, pos->py, pos->pz, pos->ow, pos->ox, pos->oy, pos->oz;

   graspsym::PoseMat goal1_mat;

   float cos45 = 0.7071;
   graspsym::PoseMat obstacle;
   obstacle << 1, 0, 0, 0.27,
             0, 1, 0, 0,
             0, 0, 1, 0,
             0, 0, 0, 1;
  
   //poseEulerToPoseMat(Ur5, poseUr5);
   graspsym::poseQuatToPoseMat(goal1_mat, goal1);
  
   graspsym::PoseMat obstaclePose;
   graspsym::PoseQuat obstaclePose_Quat;

   obstaclePose = goal1_mat * obstacle;
   graspsym::poseMatToPoseQuat (obstaclePose_Quat, obstaclePose);
   pos3D box_obstacle;
   box_obstacle.px = obstaclePose_Quat(0, 0);
   box_obstacle.py = obstaclePose_Quat(1, 0);
   box_obstacle.pz = obstaclePose_Quat(2, 0);
   box_obstacle.ow = 1;
   box_obstacle.ox = 0;
   box_obstacle.oy = 0;
   box_obstacle.oz = 0;

   // std::cout << "adding the cube" << std::endl;
   addCube(box_obstacle, 0.05, 0.05, 0.20, "obstacle_grasp");
   // std::cout << "cube added" << std::endl;

   /* 1: compute the pose of the pos-10cm in the EE-link reference frame */

   graspsym::PoseMat backOff;
   backOff << 1, 0, 0, -0.1,
             0, 1, 0, 0,
             0, 0, 1, 0,
             0, 0, 0, 1;
  
   graspsym::PoseMat backedOffPose;
   graspsym::PoseQuat backedOffPose_Quat;

   backedOffPose = goal1_mat * backOff;
   graspsym::poseMatToPoseQuat (backedOffPose_Quat, backedOffPose);

   /*std::cout << "Original: " << goal1_mat << std::endl << std::endl;
   std::cout << goal1 << std::endl << std::endl;

   std::cout << "BackedOff: " << backedOffPose << std::endl << std::endl;
   std::cout << backedOffPose_Quat << std::endl;*/

   /* 2: go to the backedOffPose_Quat */

   pos3D bo;
   bo.px = backedOffPose_Quat(0, 0);
   bo.py = backedOffPose_Quat(1, 0);
   bo.pz = backedOffPose_Quat(2, 0);
   bo.ow = backedOffPose_Quat(3, 0);
   bo.ox = backedOffPose_Quat(4, 0);
   bo.oy = backedOffPose_Quat(5, 0);
   bo.oz = backedOffPose_Quat(6, 0);

   if (moveToSync(bo, 5)) {
      std::cout << "Going for the goal." << std::endl;//TODO: perhaps change this for a (a)synchroneous method?
   }
   else {
      std::cout << "Couldn't reach the goal." << std::endl;
      return;
   }

   /* 3: make the gripper follow a cartesian path */

   std::vector<pos3D> cartesianApproach;
   getPos(&bo);
   cartesianApproach.push_back(bo);

   //removeCube("obstacle_grasp");

   pos3D final_pos(pos->px,pos->py,pos->pz,pos->ow,pos->ox,pos->oy,pos->oz);
   cartesianApproach.push_back(final_pos);
   if (moveCartesian(cartesianApproach)) {
      std::cout << "Approach successful!" << std::endl;
   }
   else {
      std::cout << "Something went wrong. :(" << std::endl;
   }
   removeCube("obstacle_grasp");

}

Arm::~Arm(){
}
